'''
	This script is used to find the topic of the doc on the basis LSI(Latent semantic indexing)

'''

import re, nltk        
from nltk.stem.porter import PorterStemmer
from nltk.corpus import stopwords
from gensim import corpora, models, similarities
import gensim

fo = open('test.txt')
stemmer = PorterStemmer()

'''
stemmer is used to bring the word to its root word for e.g. giving, gave to give
'''

def stem_tokens(tokens, stemmer):
	stemmed = []
	for item in tokens:
		stemmed.append(stemmer.stem(item))
	return stemmed

'''
	tokenize To remove all the stop words and puntuation
'''
def tokenize(text):
	text = text.lower()
	text = re.sub("[^a-zA-Z]", " ", text) 
	# text = re.sub("[http://]", " ", text)
	text = re.sub(" +"," ", text) 
	text = re.sub("\\b[a-zA-Z0-9]{10,100}\\b"," ",text) 
	text = re.sub("\\b[a-zA-Z0-9]{0,1}\\b"," ",text) 
	tokens = nltk.word_tokenize(text.strip())
	# Uncomment next line to use stemmer
	# tokens = stem_tokens(tokens, stemmer)
	tokens = nltk.pos_tag(tokens)
	return tokens

stopset = stopwords.words('english')
# freq_words = ['comment']
# for i in freq_words :
#     stopset.append(i)

text_corpus = []

for doc in fo :
	temp_doc = tokenize(doc.strip())
	current_doc = []
	for word in range(len(temp_doc)) :

		if (temp_doc[word][0] not in stopset) and (temp_doc[word][1] == 'NN' or temp_doc[word][1] == 'NNS' or temp_doc[word][1] == 'NNP' or temp_doc[word][1] == 'NNPS'):
			current_doc.append(temp_doc[word][0])

	text_corpus.append(current_doc)

dictionary = corpora.Dictionary(text_corpus)

corpus = [dictionary.doc2bow(text) for text in text_corpus]

tfidf = models.TfidfModel(corpus)
corpus = tfidf[corpus]

'''
	Making an LSI model of bag of words (corpus formed in above steps) 
'''

lsi = models.LsiModel(corpus,id2word=dictionary, num_topics=2)


'''
	this will return main topic of both the documents.
'''
for topics in lsi.print_topics(2) :      
	print topics,'\n'

'''
	A sample query given by the user to check the similarity of this query with the doc in test.txt 
	"similarities" will return the probabilty of similarity with the doc where 0th index will be for first doc and 
	1st index will be for second doc 

'''
query = ["the bank of port".lower().split(' ')]

vec_query = [dictionary.doc2bow(text) for text in query] 
index = similarities.MatrixSimilarity(lsi[corpus])
similarities = index[lsi[vec_query]]
print similarities